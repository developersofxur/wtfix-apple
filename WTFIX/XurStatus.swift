//
//  XurStatus.swift
//  WTFIX
//
//  Created by Jonathan Alexander on 2/5/24.
//

import Foundation

struct XurStatus: Codable {
    let present: Bool
    let lastUpdate: String?
    let location: XurLocation?
    func getLocationText() -> String {
        if let location = self.location {
            return "\(location.planet) > \(location.zone) > \(location.desc)"
        } else {
            return "Xûr's fucked off"
        }
    }
}

struct XurLocation: Codable {
    let planet: String
    let zone: String
    let desc: String
    let customMap: String?
    let maps: XurMaps?
}

struct XurMaps: Codable {
    let featured: String
    let site: [String: String]
    let bot: [String: String]
    let white: [String: String]
}
