//
//  WTFIXApi.swift
//  WTFIX
//
//  Created by Jonathan Alexander on 2/5/24.
//

import Foundation

class WTFIXApi {
    let WTFIXStatusUrl = URL(string: "https://api.wtfix.gg/xur/status")!
    
    func getWTFIXStatus(completion:@escaping (XurStatus) -> ()) {
            URLSession.shared.dataTask(with: WTFIXStatusUrl) { data,_,_  in
                let status = try! JSONDecoder().decode(XurStatus.self, from: data!)
                
                DispatchQueue.main.async {
                    completion(status)
                }
            }
            .resume()
        }
}
