//
//  ContentView.swift
//  WTFIX
//
//  Created by Jonathan Alexander on 2/3/24.
//

import SwiftUI
import RealityKit
import RealityKitContent

struct ContentView: View {
    @State private var status: XurStatus?
    @State private var showMap = false
    @State private var showInfoPopup = false
    @State private var refreshing = false
    
    var body: some View {
        VStack(spacing: 0) {
            if let status = self.status {
                ZStack {
                    Text(status.getLocationText()).font(.largeTitle)
                    VStack {
                        HStack {
                            Button(action: {
                                self.refreshing = true
                                WTFIXApi().getWTFIXStatus{ (status) in
                                    self.status = status
                                    self.refreshing = false
                                }
                            }) {
                                Image(systemName: "arrow.clockwise")
                            }
                            .buttonBorderShape(.circle)
                            if self.refreshing {
                                ProgressView().padding(.horizontal)
                            }
                            Spacer()
                            Button(action: {
                                showInfoPopup = true
                            }) {
                                Image(systemName: "info")
                            }
                            .buttonBorderShape(.circle)
                            .sheet(isPresented: $showInfoPopup) {
                                ZStack {
                                    VStack {
                                        HStack {
                                            Spacer()
                                            Button(action: {
                                                showInfoPopup = false
                                            }) {
                                                Image(systemName: "xmark")
                                            }.buttonBorderShape(.circle)
                                        }
                                        Spacer()
                                    }
                                    VStack {
                                        Image("WTFIX")
                                            .resizable()
                                            .scaledToFit()
                                            .frame(width: 200, height: 200)
                                        Text("Made with ❤️ by the WTFIX team")
                                            .padding()
                                        Button(action: {
                                            if let url = URL(string: "https://wtfix.gg") {
                                                UIApplication.shared.open(url)
                                            }
                                        }) {
                                            Label("WTFIX", systemImage: "globe")
                                        }
                                        Button(action: {
                                            if let url = URL(string: "https://bungiepls.com") {
                                                UIApplication.shared.open(url)
                                            }
                                        }) {
                                            Label("Bungie Pls", systemImage: "newspaper")
                                        }
                                        Button(action: {
                                            if let url = URL(string: "https://x.com/wtfixur") {
                                                UIApplication.shared.open(url)
                                            }
                                        }) {
                                            Label("X (Twitter)", systemImage: "x.square")
                                        }
                                        Button(action: {
                                            if let url = URL(string: "https://instagram.com/wtfixur") {
                                                UIApplication.shared.open(url)
                                            }
                                        }) {
                                            Label("Instagram", systemImage: "photo")
                                        }
                                        
                                    }
                                }.padding()
                            }
                        }
                        Spacer()
                        HStack {
                            Spacer()
                            if (self.status?.present == true) {
                                Button(action: {
                                    self.showMap = !self.showMap
                                }) {
                                    Image(systemName: "map")
                                }.buttonBorderShape(.circle)
                            }
                        }
                    }
                }
                .padding()
                .frame(height: 200)
                if (status.location != nil && self.showMap) {
                    if let maps = status.location?.maps {
                        if (self.showMap) {
                            if let mapUrl = maps.white[maps.featured] {
                                VStack {
                                    Spacer()
                                    AsyncImage(url: URL(string: "https://wtfix.gg" + mapUrl)) { image in
                                        image.resizable()
                                            .aspectRatio(contentMode: .fit)
                                        
                                    } placeholder: {
                                        HStack {
                                            Spacer()
                                            ProgressView()
                                            Spacer()
                                        }
                                    }
                                    Spacer()
                                }.background(Color.black.opacity(0.5))
                            }
                        }
                    }
                }
            } else {
                ProgressView()
            }
        }
        .onAppear() {
            WTFIXApi().getWTFIXStatus{ (status) in
                self.status = status
            }
        }
        .frame(minWidth: 600, maxWidth: 600, minHeight: self.status?.present == true && self.showMap ? 800 : 200, maxHeight: self.status?.present == true && self.showMap ? 800 : 200)
    }
}

#Preview(windowStyle: .automatic) {
    ContentView()
}
