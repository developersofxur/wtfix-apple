//
//  WTFIXApp.swift
//  WTFIX
//
//  Created by Jonathan Alexander on 2/3/24.
//

import SwiftUI

@main
struct WTFIXApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }.windowResizability(.contentSize)
    }
}
